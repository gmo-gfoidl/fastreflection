﻿using System;
using System.Diagnostics;
using System.Reflection;
using System.Reflection.Emit;

namespace gfoidl.FastReflection
{
#if !DEBUG
	[DebuggerNonUserCode]
#endif
	internal static class DynamicMethodHelper
	{
		public static FastMethod CreateFastMethod(Key key)
		{
			Type target 	  = key.Type;
			string methodName = key.MemberName;

			MethodInfo mi 		  = target.GetMethod(methodName);
			ParameterInfo[] parms = mi.GetParameters();
			Type[] argTypes 	  = { typeof(object), typeof(object[]) };
			DynamicMethod dm 	  = new DynamicMethod("_" + methodName + "_", typeof(object), argTypes, target);
			ILGenerator ilGen 	  = dm.GetILGenerator();

			Label argsOK = ilGen.DefineLabel();

			ilGen.Emit(OpCodes.Ldarg_1);
			ilGen.Emit(OpCodes.Ldlen);
			ilGen.Emit(OpCodes.Ldc_I4, parms.Length);
			ilGen.Emit(OpCodes.Beq, argsOK);

			ilGen.Emit(OpCodes.Newobj, typeof(TargetParameterCountException).GetConstructor(Type.EmptyTypes));
			ilGen.Emit(OpCodes.Throw);

			ilGen.MarkLabel(argsOK);

			if (!mi.IsStatic) ilGen.Emit(OpCodes.Ldarg_0);

			for (int i = 0; i < parms.Length; ++i)
			{
				ilGen.Emit(OpCodes.Ldarg_1);
				ilGen.Emit(OpCodes.Ldc_I4, i);
				ilGen.Emit(OpCodes.Ldelem_Ref);

				if (parms[i].ParameterType.IsValueType) 
					ilGen.Emit(OpCodes.Unbox_Any, parms[i].ParameterType);
			}

			if (mi.IsVirtual)
				ilGen.Emit(OpCodes.Callvirt, mi);
			else
				ilGen.Emit(OpCodes.Call, mi);

			if (mi.ReturnType == typeof(void))
				ilGen.Emit(OpCodes.Ldnull);
			else
			{
				if (mi.ReturnType.IsValueType)
					ilGen.Emit(OpCodes.Box, mi.ReturnType);
			}

			ilGen.Emit(OpCodes.Ret);

			return dm.CreateDelegate(typeof(FastMethod)) as FastMethod;
		}
		//---------------------------------------------------------------------
		public static FastSetter CreateFastSetter(Key key)
		{
			Type target 		= key.Type;
			string propertyName = key.MemberName;

			PropertyInfo pi 	 = target.GetProperty(propertyName);
			MethodInfo setMethod = pi.GetSetMethod();

			if (setMethod == null) return null;

			Type[] arguments  = { typeof(object), typeof(object) };
			DynamicMethod dm  = new DynamicMethod("_Set" + propertyName + "_", typeof(void), arguments, target);
			ILGenerator ilGen = dm.GetILGenerator();

			ilGen.Emit(OpCodes.Ldarg_0);
			ilGen.Emit(OpCodes.Ldarg_1);

			if (pi.PropertyType.IsValueType)
				ilGen.Emit(OpCodes.Unbox_Any, pi.PropertyType);

			ilGen.EmitCall(OpCodes.Callvirt, setMethod, null);
			ilGen.Emit(OpCodes.Ret);

			return dm.CreateDelegate(typeof(FastSetter)) as FastSetter;
		}
		//---------------------------------------------------------------------
		public static FastGetter CreateFastGetter(Key key)
		{
			Type target 		= key.Type;
			string propertyName = key.MemberName;

			PropertyInfo pi 	 = target.GetProperty(propertyName);
			MethodInfo getMethod = pi.GetGetMethod();

			if (getMethod == null) return null;

			Type[] arguments  = { typeof(object) };
			DynamicMethod dm  = new DynamicMethod("_Get" + propertyName + "_", typeof(object), arguments, target);
			ILGenerator ilGen = dm.GetILGenerator();

			ilGen.Emit(OpCodes.Ldarg_0);
			ilGen.Emit(OpCodes.Callvirt, getMethod);

			if (pi.PropertyType.IsValueType)
				ilGen.Emit(OpCodes.Box, pi.PropertyType);

			ilGen.Emit(OpCodes.Ret);

			return dm.CreateDelegate(typeof(FastGetter)) as FastGetter;
		}
	}
}