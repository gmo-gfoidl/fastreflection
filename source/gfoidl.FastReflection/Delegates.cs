﻿namespace gfoidl.FastReflection
{
	public delegate object FastMethod(object instance, params object[] args);
	public delegate void   FastSetter(object target, object value);
	public delegate object FastGetter(object target);
}