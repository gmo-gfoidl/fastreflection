﻿using System;
using System.Collections.Concurrent;
using System.Diagnostics;

namespace gfoidl.FastReflection
{
#if !DEBUG
	[DebuggerNonUserCode]
#endif
	public static class FastReflectionHelper
	{
		private static ConcurrentDictionary<Key, FastMethod> _methodCache = new ConcurrentDictionary<Key, FastMethod>();
		private static ConcurrentDictionary<Key, FastSetter> _setterCache = new ConcurrentDictionary<Key, FastSetter>();
		private static ConcurrentDictionary<Key, FastGetter> _getterCache = new ConcurrentDictionary<Key, FastGetter>();
		//---------------------------------------------------------------------
		public static FastMethod GetFastMethod(Type target, string methodName)
		{
			Key key = GetKey(target, methodName);
			return _methodCache.GetOrAdd(key, DynamicMethodHelper.CreateFastMethod);
		}
		//---------------------------------------------------------------------
		public static FastSetter GetFastSetter(Type target, string propertyName)
		{
			Key key = GetKey(target, propertyName);
			return _setterCache.GetOrAdd(key, DynamicMethodHelper.CreateFastSetter);
		}
		//---------------------------------------------------------------------
		public static FastGetter GetFastGetter(Type target, string propertyName)
		{
			Key key = GetKey(target, propertyName);
			return _getterCache.GetOrAdd(key, DynamicMethodHelper.CreateFastGetter);
		}
		//---------------------------------------------------------------------
		private static Key GetKey(Type target, string memberName)
		{
			target	  .CheckNotNull(() => target);
			memberName.CheckStringNotNullOrWhitespace(() => memberName);

			Key key = new Key(target, memberName);
			return key;
		}
	}
}