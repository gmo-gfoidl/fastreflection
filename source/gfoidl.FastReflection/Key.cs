﻿using System;
using System.Diagnostics;

namespace gfoidl.FastReflection
{
	[DebuggerNonUserCode]
	internal struct Key : IEquatable<Key>
	{
		public Type Type 		 { get; private set; }
		public string MemberName { get; private set; }
		//---------------------------------------------------------------------
		public Key(Type type, string memberName) : this()
		{
			this.Type 		= type;
			this.MemberName = memberName;
		}
		//---------------------------------------------------------------------
		#region IEquatable<Key> Members
		public bool Equals(Key other)
		{
			return this.Type == other.Type && this.MemberName == other.MemberName;
		}
		//---------------------------------------------------------------------
		public override bool Equals(object obj)
		{
			if (obj != null && obj is Key) return this.Equals((Key)obj);

			return false;
		}
		//---------------------------------------------------------------------
		public override int GetHashCode()
		{
			return this.Type.GetHashCode() ^ this.MemberName.GetHashCode();
		}
		#endregion
	}
}