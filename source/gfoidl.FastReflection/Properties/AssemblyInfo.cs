﻿using System.Reflection;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("gfoidl.FastReflection")]
[assembly: AssemblyDescription("Helper for boosting speed of .net Reflection")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyProduct("gfoidl.FastReflection")]
[assembly: AssemblyCulture("")]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("84dca63b-e296-4879-b05e-9c303c8567b2")]