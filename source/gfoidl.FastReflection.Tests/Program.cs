﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace gfoidl.FastReflection.Tests
{
	public class Program
	{
		static void Main()
		{
			var test = new FastReflectionHelperTest();
			test.GetFastMethod_StaticMethod_ReturnsCorrectDelegate();
		}
	}
}