﻿using NUnit.Framework;
using System;

namespace gfoidl.FastReflection.Tests
{
	[TestFixture]
	public class FastReflectionHelperTest
	{
		#region GetFastMethod
		[Test]
		[ExpectedException(typeof(ArgumentNullException))]
		public void GetFastMethod_TypeIsNull_ThrowsArgumentNull()
		{
			FastMethod method = FastReflectionHelper.GetFastMethod(null, "DoubleName");
		}
		//---------------------------------------------------------------------
		[Test]
		[ExpectedException(typeof(ArgumentNullException))]
		public void GetFastMethod_MethodNameIsNullOrEmpty_ThrowsArgumentNull([Values(null, "", " ")]string methodName)
		{
			FastMethod method = FastReflectionHelper.GetFastMethod(typeof(TestObject), methodName);
		}
		//---------------------------------------------------------------------
		[Test]
		public void GetFastMethod_VoidNoArgs_ReturnsCorrectDelegate()
		{
			FastMethod method = FastReflectionHelper.GetFastMethod(typeof(TestObject), "DoubleName");
			TestObject obj 	  = new TestObject { Name = "abc" };

			method(obj);

			Assert.AreEqual("abcabc", obj.Name);
		}
		//---------------------------------------------------------------------
		[Test]
		public void GetFastMethod_VoidReferenceTypeArg_ReturnsCorrectDelegate()
		{
			FastMethod method = FastReflectionHelper.GetFastMethod(typeof(TestObject), "SetName");
			TestObject obj 	  = new TestObject();

			method(obj, "Himen");

			Assert.AreEqual("Himen", obj.Name);
		}
		//---------------------------------------------------------------------
		[Test]
		public void GetFastMethod_VoidValueTypeArg_ReturnsCorrectDelegate()
		{
			FastMethod method = FastReflectionHelper.GetFastMethod(typeof(TestObject), "SetAge");
			TestObject obj 	  = new TestObject();

			method(obj, 42);

			Assert.AreEqual(42, obj.Age);
		}
		//---------------------------------------------------------------------
		[Test]
		public void GetFastMethod_ReturnsReferenceTypeNoArgs_ReturnsCorrectDelegate()
		{
			FastMethod method = FastReflectionHelper.GetFastMethod(typeof(TestObject), "GetDoubledName");
			TestObject obj 	  = new TestObject { Name = "xyz" };

			string actual = method(obj) as string;

			Assert.AreEqual("xyzxyz", actual);
		}
		//---------------------------------------------------------------------
		[Test]
		public void GetFastMethod_ReturnsValueTypeNoArgs_ReturnsCorrectDelegate()
		{
			FastMethod method = FastReflectionHelper.GetFastMethod(typeof(TestObject), "GetDoubledAge");
			TestObject obj 	  = new TestObject { Age = 42 };

			int actual = (int)method(obj);

			Assert.AreEqual(84, actual);
		}
		//---------------------------------------------------------------------
		[Test]
		public void GetFastMethod_ReturnsReferenceTypeReferenceTypeArg_ReturnsCorrectDelegate()
		{
			FastMethod method = FastReflectionHelper.GetFastMethod(typeof(TestObject), "GetConcatenatedName");
			TestObject obj 	  = new TestObject { Name = "abc" };

			string actual = method(obj, "xyz") as string;

			Assert.AreEqual("abcxyz", actual);
		}
		//---------------------------------------------------------------------
		[Test]
		public void GetFastMethod_ReturnsValueTypeValueTypeArg_ReturnsCorrectDelegate()
		{
			FastMethod method = FastReflectionHelper.GetFastMethod(typeof(TestObject), "GetSummedAge");
			TestObject obj 	  = new TestObject { Age = 100 };

			int actual = (int)method(obj, 42);

			Assert.AreEqual(142, actual);
		}
		//---------------------------------------------------------------------
		[Test]
		public void GetFastMethod_StaticMethod_ReturnsCorrectDelegate()
		{
			FastMethod method = FastReflectionHelper.GetFastMethod(typeof(TestObject), "ToUpper");

			string actual = method(null, "abcd") as string;

			Assert.AreEqual("ABCD", actual);
		}
		#endregion
		//---------------------------------------------------------------------
		#region GetFastSetter
		[Test]
		[ExpectedException(typeof(ArgumentNullException))]
		public void GetFastSetter_TypeIsNull_ThrowsArgumentNull()
		{
			FastSetter setter = FastReflectionHelper.GetFastSetter(null, "Name");
		}
		//---------------------------------------------------------------------
		[Test]
		[ExpectedException(typeof(ArgumentNullException))]
		public void GetFastSetter_PropertyNameIsNullOrEmtpy_ThrowsArgumentNull([Values(null, "", " ")]string propertyName)
		{
			FastSetter setter = FastReflectionHelper.GetFastSetter(typeof(TestObject), propertyName);
		}
		//---------------------------------------------------------------------
		[Test]
		public void GetFastSetter_ReferenceTypeProperty_ReturnsCorrectDelegate()
		{
			FastSetter setter = FastReflectionHelper.GetFastSetter(typeof(TestObject), "Name");
			TestObject obj 	  = new TestObject();

			setter(obj, "Gü");

			Assert.AreEqual("Gü", obj.Name);
		}
		//---------------------------------------------------------------------
		[Test]
		public void GetFastSetter_ValueTypeProperty_ReturnsCorrectDelegate()
		{
			FastSetter setter = FastReflectionHelper.GetFastSetter(typeof(TestObject), "Age");
			TestObject obj 	  = new TestObject();

			setter(obj, 30);

			Assert.AreEqual(30, obj.Age);
		}
		#endregion
		//---------------------------------------------------------------------
		#region GetFastGetter
		[Test]
		[ExpectedException(typeof(ArgumentNullException))]
		public void GetFastGetter_TypeIsNull_ThrowsArgumentNull()
		{
			FastGetter getter = FastReflectionHelper.GetFastGetter(null, "Name");
		}
		//---------------------------------------------------------------------
		[Test]
		[ExpectedException(typeof(ArgumentNullException))]
		public void GetFastGetter_PropertyNameIsNullOrEmtpy_ThrowsArgumentNull([Values(null, "", " ")]string propertyName)
		{
			FastGetter getter = FastReflectionHelper.GetFastGetter(typeof(TestObject), propertyName);
		}
		//---------------------------------------------------------------------
		[Test]
		public void GetFastGetter_ReferenceTypeProperty_ReturnsCorrectDelegate()
		{
			FastGetter getter = FastReflectionHelper.GetFastGetter(typeof(TestObject), "Name");
			TestObject obj 	  = new TestObject();
			obj.Name 		  = "Anton";

			object actual = getter(obj);

			Assert.AreEqual("Anton", actual);
		}
		//---------------------------------------------------------------------
		[Test]
		public void GetFastGetter_ValueTypeProperty_ReturnsCorrectDelegate()
		{
			FastGetter getter = FastReflectionHelper.GetFastGetter(typeof(TestObject), "Age");
			TestObject obj 	  = new TestObject();
			obj.Age 		  = 42;

			object actual = getter(obj);

			Assert.AreEqual(42, actual);
		}
		#endregion
	}
}