﻿using System.IO;
using NUnit.Framework;

namespace gfoidl.FastReflection.Tests
{
	[TestFixture]
	public class KeyTest
	{
		[Test]
		public void Ctor_ArgsGiven_CorrectPropertiesSet()
		{
			Key sut = new Key(typeof(MemoryStream), "Read");

			Assert.AreEqual(typeof(MemoryStream), sut.Type);
			Assert.AreEqual("Read", sut.MemberName);
		}
		//---------------------------------------------------------------------
		[Test]
		public void Equals_ObjectNull_ReturnsFalse()
		{
			Key sut = new Key(typeof(MemoryStream), "Read");

			Assert.IsFalse(sut.Equals(null));
		}
		//---------------------------------------------------------------------
		[Test]
		public void Equals_ObjectOfDifferentType_ReturnsFalse()
		{
			Key sut = new Key(typeof(MemoryStream), "Read");
			object obj = new object();

			Assert.IsFalse(sut.Equals(obj));
		}
		//---------------------------------------------------------------------
		[Test]
		public void Equals_ObjectOfSameInstance_ReturnsTrue()
		{
			Key sut = new Key(typeof(MemoryStream), "Read");
			object obj = sut;

			Assert.IsTrue(sut.Equals(obj));
		}
		//---------------------------------------------------------------------
		[Test]
		public void Equals_WithItSelf_ReturnsTrue()
		{
			Key sut = new Key(typeof(MemoryStream), "Read");

			Assert.IsTrue(sut.Equals(sut));
		}
		//---------------------------------------------------------------------
		[Test]
		public void Equals_WithCoypOfItSelf_ReturnsTrue()
		{
			Key sut = new Key(typeof(MemoryStream), "Read");
			Key key = new Key(typeof(MemoryStream), "Read");

			Assert.IsTrue(sut.Equals(key));
		}
		//---------------------------------------------------------------------
		[Test]
		public void Equals_DifferentType_ReturnsFalse()
		{
			Key sut = new Key(typeof(MemoryStream), "Read");
			Key key = new Key(typeof(FileStream), "Read");

			Assert.IsFalse(sut.Equals(key));
		}
		//---------------------------------------------------------------------
		[Test]
		public void Equals_DifferentMemberName_ReturnsFalse()
		{
			Key sut = new Key(typeof(MemoryStream), "Read");
			Key key = new Key(typeof(MemoryStream), "Write");

			Assert.IsFalse(sut.Equals(key));
		}
		//---------------------------------------------------------------------
		[Test]
		public void GetHashCode_WithItSelf_ReturnsSameHashCode()
		{
			Key sut = new Key(typeof(MemoryStream), "Read");

			Assert.AreEqual(sut.GetHashCode(), sut.GetHashCode());
		}
		//---------------------------------------------------------------------
		[Test]
		public void GetHashCode_WithCopyOfItSelf_ReturnsSameHashCode()
		{
			Key sut = new Key(typeof(MemoryStream), "Read");
			Key key = new Key(typeof(MemoryStream), "Read");

			Assert.AreEqual(sut.GetHashCode(), key.GetHashCode());
		}
		//---------------------------------------------------------------------
		[Test]
		public void GetHashCode_DifferentType_ReturnsDifferentHashCode()
		{
			Key sut = new Key(typeof(MemoryStream), "Read");
			Key key = new Key(typeof(FileStream), "Read");

			Assert.AreNotEqual(sut.GetHashCode(), key.GetHashCode());
		}
		//---------------------------------------------------------------------
		[Test]
		public void GetHashCode_DifferentMemberName_ReturnsDifferentHashCode()
		{
			Key sut = new Key(typeof(MemoryStream), "Read");
			Key key = new Key(typeof(MemoryStream), "Write");

			Assert.AreNotEqual(sut.GetHashCode(), key.GetHashCode());
		}
	}
}