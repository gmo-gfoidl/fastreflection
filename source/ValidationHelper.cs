﻿using System;
using System.Diagnostics;
using System.Diagnostics.Contracts;
using System.Linq.Expressions;

namespace gfoidl.FastReflection
{
	[DebuggerNonUserCode]
	internal static class ValidationHelper
	{
		[ContractArgumentValidator]
		public static void CheckNotNull<T>(this T arg, Expression<Func<T>> exp)
		{
			if (arg == null) throw new ArgumentNullException(GetParamName(exp));
			Contract.EndContractBlock();
		}
		//---------------------------------------------------------------------
		[ContractArgumentValidator]
		public static void CheckStringNotNullOrWhitespace(this string arg, Expression<Func<string>> exp)
		{
			if (string.IsNullOrWhiteSpace(arg)) throw new ArgumentNullException(GetParamName(exp));
			Contract.EndContractBlock();
		}
		//---------------------------------------------------------------------
		private static string GetParamName<T>(Expression<Func<T>> exp)
		{
			MemberExpression me = exp.Body as MemberExpression;
			return me.Member.Name;
		}
	}
}